public class Departamento {
    public int dept_no;
    public String dnombre;
    public String loc;

    public Departamento(){}
    public Departamento(int deptno, String dnombre, String loc) {
        this.dept_no = deptno;
        this.dnombre = dnombre;
        this.loc = loc;
    }

    public int getDeptno() {
        return dept_no;
    }

    public void setDeptno(int deptno) {
        this.dept_no = deptno;
    }

    public String getDnombre() {
        return dnombre;
    }

    public void setDnombre(String dnombre) {
        this.dnombre = dnombre;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }
}
