public abstract class DAOFactory {
    //Bases de datos soportadas
    public static final int POSTGRESQL = 1;
    public static final int MONGODB = 2;

    public abstract DepartamentoDAO getDepartamentoDAO();
    public abstract EmpleadosDAO getEmpleadoDAO();

    public static DAOFactory getDAOFactory(int bd){
        switch (bd){
            case POSTGRESQL:
                return new SqlDbDAOFactory();
            case MONGODB:
                return null; //new MongoDBDAOFactory();
            default:
                return null;
        }
    }
}
