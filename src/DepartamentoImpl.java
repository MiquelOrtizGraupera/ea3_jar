import conexio.ConexionSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DepartamentoImpl implements DepartamentoDAO {
    Connection conexion;

    {
        try {
            conexion = ConexionSQL.conexioSQL();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    public DepartamentoImpl(){
//        try{
//           conexion = ConexionSQL.conexioSQL();
//
//        }catch (SQLException ex){
//            System.out.println("Error: "+ ex.getErrorCode());
//        }
//    }

    @Override
    public boolean insertarDep(Departamento dep) {
        boolean valor = false;
        String querySQL = "INSERT INTO departamentos VALUES(?,?,?)";
        PreparedStatement sentencia;
        try{
            sentencia = conexion.prepareStatement(querySQL);
            sentencia.setInt(1, dep.getDeptno());
            sentencia.setString(2, dep.getDnombre());
            sentencia.setString(3, dep.getLoc());
            int filas = sentencia.executeUpdate();
            System.out.printf("Filas insertadas: %d%n",filas);
            if(filas > 0){
                valor = true;
                System.out.printf("Departamento %d insertado%n", dep.getDeptno());
            }
            sentencia.close();
        }catch (SQLException e){
            System.err.println(e.getErrorCode());
        }
        return valor;
    }

    @Override
    public boolean eliminarDep(int deptno) {
        boolean valor = false;
        String querySQL = "DELETE FROM departamentos WHERE dept_no = ?";
        PreparedStatement sentencia;
        try{
            sentencia = conexion.prepareStatement(querySQL);
            sentencia.setInt(1, deptno);
            int filas = sentencia.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Departamento %d eliminado%n", deptno);
            }
            sentencia.close();
        }catch (SQLException e){
            System.err.println(e.getErrorCode());
        }
        return valor;
    }

    @Override
    public boolean modificarDep(Departamento dep) {
        boolean valor = false;
        String querySQL = "UPDATE departamentos SET dnombre = ?, loc = ? WHERE dept_no = ?";
        PreparedStatement sentencia;
        try {
            sentencia = conexion.prepareStatement(querySQL);
            sentencia.setInt(3,dep.getDeptno());
            sentencia.setString(1, dep.getDnombre());
            sentencia.setString(2, dep.getLoc());
            int filas = sentencia.executeUpdate();
            System.out.printf("Filas modificadas: %d%n",filas);
            if(filas > 0){
                valor = true;
                System.out.printf("Departamento %d  modificado%n",dep.getDeptno());
            }
            sentencia.close();
        }catch (SQLException e){
            e.getErrorCode();
        }
        return valor;
    }

    @Override
    public Departamento consultarDep(int deptno) {
        String querySQL = "SELECT dept_no, dnombre, loc FROM departamentos WHERE dept_no = ?";
        PreparedStatement sentencia;
        Departamento dep = new Departamento();
        try{
            sentencia = conexion.prepareStatement(querySQL);
            sentencia.setInt(1, deptno);
            ResultSet result = sentencia.executeQuery();
            if(result.next()){
                dep.setDeptno(result.getInt("dept_no"));
                dep.setDnombre(result.getString("dnombre"));
                dep.setLoc(result.getString("loc"));
            }else{
                System.out.printf("Departamento: %d no existe%n", deptno);
            }
            sentencia.close();
        }catch (SQLException e){
            e.getErrorCode();
        }
        return dep;
    }
}
