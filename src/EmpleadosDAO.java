public interface EmpleadosDAO {
    public boolean insertarEmp(Empleado emp);
    public boolean eliminarEmp(int emp_no);
    public boolean modificarEmp(Empleado emp);
    public Empleado consultarEmp(int emp_no);
}
