import conexio.ConexionSQL;

import java.sql.*;

public class EmpleadosImpl implements EmpleadosDAO{
    Connection connection;

    {
        try{
            connection = ConexionSQL.conexioSQL();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean insertarEmp(Empleado emp) {
        boolean valor = false;
        String querySQL = "INSERT INTO empleados VALUES(?,?,?,?,?,?,?,?)";
        PreparedStatement sentencia;
        try{
            sentencia = connection.prepareStatement(querySQL);
            sentencia.setInt(1,emp.getEmp_no());
            sentencia.setString(2,emp.getApellido());
            sentencia.setString(3,emp.getOficio());
            sentencia.setInt(4,emp.getDir());
            sentencia.setString(5,emp.getFecha_alt());
            sentencia.setDouble(6,emp.getSalario());
            sentencia.setDouble(7,emp.getComision());
            sentencia.setInt(8,emp.getDept_no());
            int filas = sentencia.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Empleado %d insertado%n",emp.getEmp_no());
            }
            sentencia.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }



    @Override
    public boolean eliminarEmp(int emp_no) {
        boolean valor = false;
        String querySQL = "DELETE FROM empleados  WHERE emp_no = ?";
        PreparedStatement sentencia;
        try {
            sentencia = connection.prepareStatement(querySQL);
            sentencia.setInt(1, emp_no);
            int filas = sentencia.executeUpdate();
            if(filas > 0){
                valor = true;
                System.out.printf("Empleado %d eliminado%n", emp_no);
            }
            sentencia.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }


    @Override
    public boolean modificarEmp(Empleado emp) {
        boolean valor = false;
        String querySQL = "UPDATE empleados SET apellido = ?,oficio = ?, dir = ?, fecha_alt = ?," +
                " salario = ?, comision = ?, dept_no = ? WHERE emp_no = ?";
        PreparedStatement sentencia;
        try{
            sentencia = connection.prepareStatement(querySQL);
            sentencia.setInt(8,emp.getEmp_no());
            sentencia.setString(1,emp.getApellido());
            sentencia.setString(2, emp.getOficio());
            sentencia.setInt(3,emp.getDir());
            sentencia.setString(4,emp.getFecha_alt());
            sentencia.setDouble(5,emp.getSalario());
            sentencia.setDouble(6,emp.getComision());
            sentencia.setInt(7,emp.getDept_no());
            int filas = sentencia.executeUpdate();
            System.out.printf("Filas modificadas: %d%n",filas);
            if(filas > 0){
                valor = true;
                System.out.printf("Empleado %s modificado%n",emp.getApellido());
            }
            sentencia.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return valor;
    }



    @Override
    public Empleado consultarEmp(int emp_no) {
        String querySQL = "SELECT * from empleados WHERE emp_no = ?";
        PreparedStatement sentencia;
        Empleado emp = new Empleado();
        try{
            sentencia = connection.prepareStatement(querySQL);
            sentencia.setInt(1, emp_no);
            ResultSet resultat = sentencia.executeQuery();
            if(resultat.next()){
                emp.setEmp_no(resultat.getInt("emp_no"));
                emp.setApellido(resultat.getString("apellido"));
                emp.setOficio(resultat.getString("oficio"));
                emp.setDir(resultat.getInt("dir"));
                emp.setFecha_alt(resultat.getString("fecha_alt"));
                emp.setSalario(resultat.getDouble("salario"));
                emp.setComision(resultat.getDouble("comision"));
                emp.setDept_no(resultat.getInt("dept_no"));
            }else{
                System.out.printf("El empleado %d no Existe!%n",emp_no);
            }
            sentencia.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return emp;
    }
}
