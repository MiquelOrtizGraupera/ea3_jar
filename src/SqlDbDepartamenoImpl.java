import conexio.ConexionSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlDbDepartamenoImpl implements DepartamentoDAO {
    Connection connection;

    {
        try {
            connection = ConexionSQL.conexioSQL();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean insertarDep(Departamento dep){
        boolean valor = false;
        String querySQL = "INSERT INTO departamentos  VALUES (?,?,?)";
        PreparedStatement sentencia;
        try{
            sentencia = connection.prepareStatement(querySQL);
            sentencia.setInt(1,dep.getDeptno());
            sentencia.setString(2,dep.getDnombre());
            sentencia.setString(3,dep.getLoc());
            int filas = sentencia.executeUpdate();
            System.out.println("Filas insertadas: "+filas);
            if(filas > 0){
                valor = true;
                System.out.printf("Departamento; %d insertado%n",dep.getDeptno());
            }
            sentencia.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return valor;
    }

    @Override
    public boolean eliminarDep(int deptno) {
        boolean valor = false;
        String querySQL = "DELETE FROM departamentos WHERE dept_no = ?";
        PreparedStatement statement;
        try{
            statement = connection.prepareStatement(querySQL);
            statement.setInt(1,deptno);
            int filas = statement.executeUpdate();
            System.out.printf("Filas eliminadas: $d%n", filas);
            if(filas > 0){
                valor = true;
                System.out.printf("Departamento %d eliminado%n",deptno);
            }
            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return valor;
    }

    @Override
    public boolean modificarDep(Departamento dep) {
        boolean valor = false;
        String querySQL = "UPDATE departamentos SET dnombre = ?, loc = ? WHERE dept_no = ?";
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(querySQL);
            statement.setInt(3, dep.getDeptno());
            statement.setString(1,dep.getDnombre());
            statement.setString(2,dep.getLoc());
            int filas = statement.executeUpdate();
            System.out.println("Filas modificadas: "+ filas);
            if(filas > 0){
                valor = true;
                System.out.printf("Departamento %d modificado%n",dep.getDeptno());
            }
            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return valor;
    }

    @Override
    public Departamento consultarDep(int deptno) {
        String querySQL = "SELECT dept_no, dnombre, loc FROM departamentos WHERE dept_no = ?";
        PreparedStatement statement;
        Departamento dep = new Departamento();
        try{
            statement = connection.prepareStatement(querySQL);
            statement.setInt(1,deptno);
            ResultSet result = statement.executeQuery();
            if(result.next()){
                dep.setDeptno(result.getInt("dept_no"));
                dep.setDnombre(result.getString("dnombre"));
                dep.setLoc(result.getString("loc"));
            }else{
                System.out.println("Departamento "+deptno+ " no existe" );
            }
            result.close();
            statement.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return dep;
    }
}
