import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SqlDbDAOFactory extends DAOFactory {
    static Connection connection = null;
    static String DRIVER = "org.postgresql.Driver";
    static String URLDB = "jdbc:postgresql://tyke.db.elephantsql.com:5432/xlbtkqld";
    static String USUARIO = "xlbtkqld";
    static String CLAVE = "ZFP3rzsriIHB92Ha2YuMPwABDcuq9mZ0";

    public SqlDbDAOFactory(){
        DRIVER = "org.postgresql.Driver";
        URLDB = "jdbc:postgresql://tyke.db.elephantsql.com:5432/xlbtkqld";
    }

    //Crear conexion
    public static Connection crearConexion(){
        if(connection == null){
            try{
                Class.forName(DRIVER);
            }catch (ClassNotFoundException ex){
                Logger.getLogger(SqlDbDAOFactory.class.getName()).log(Level.SEVERE,null,ex);
            }
            try{
                connection = DriverManager.getConnection(URLDB,USUARIO,CLAVE);
            }catch (SQLException ex){
                System.err.println("HA OCURRIDO UNA EXECEPCIÓN");
                System.err.println(ex.getMessage());
            }
        }
        return connection;
    }

    @Override
    public DepartamentoDAO getDepartamentoDAO() {
        return new SqlDbDepartamenoImpl();
    }

    @Override
    public EmpleadosDAO getEmpleadoDAO() {
        return new SqlDbEmpleadosImpl();
    }


}
